const airtable = require('airtable');
const bases = require('./bases.json');
const key = require('./.airtable-api-key');
const debug = false;

function configureAirtable(base) {

  airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: key
  })

}

function prettyPrintObject(object, fields) {
  if (!fields || !fields.length) fields = Object.keys(object);
  fields.forEach(key => {
    console.log(`${key}:
    ${object[key]}
    `)
  })
  console.log('==============================');
}

function printCSVObject(object, fields) {
  if (!fields || !fields.length) fields = Object.keys(object);
  console.log(fields.map(k => (object[k] || '').toString()).join(','))
}

function main() {

  // base.table.view.field1,field2#1
  // e.g., better-life.Weeks.5/3 Balance (Saturday)

  if (debug) console.log('Bases:', bases);

  const query = process.argv[2];
  format = process.argv[3] || 'json';

  if (debug) console.log('Query', query)

  const [baseAlias, tableOpts, fieldString] = query.split('.')

  if (debug)
    console.log(`baseAlias=${baseAlias}, tableOpts=${tableOpts}, fieldString=${fieldString}`)

  fields = (!!fieldString) ? fieldString.split(',') : [] ;

  const [table, view] = tableOpts.split('@');

  configureAirtable();
  var base = airtable.base(bases[baseAlias].id);

  var recordCallback = null;
  var postFetchCallback = null;
  var accumulator = null;

  switch (format) {
    case 'json': {
      accumulator = [];
      recordCallback = (r) => accumulator.push(r);
      postFetchCallback = (a) => console.log(a);
      break;
    }
    case 'csv': recordCallback = printCSVObject; break;
    default: recordCallback = prettyPrintObject; break;
  }

  base(table).select({
    fields: fields || undefined,
    view: view || ''
  }).eachPage(function(records, next) {
    records.forEach(r => recordCallback(r.fields, fields));
    next();
  }, function(err) {
    if (err) console.error(err);
    else if (typeof(postFetchCallback) === 'function') postFetchCallback(accumulator);
  });

}

main();
