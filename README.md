# Airtable Integration

A simple-to-use AirTable fetching CLI. This is a just-for-fun project so things
might not be as ironclad as they should be for serious use.

## Setup

Before you do anything, install the dependencies:

    npm install

Then configure the CLI:

1. Get an API key from AirTable and `module.exports` it in `.airtable-api-key`, and
2. Get the IDs of the bases you want to use, and plunk them into `bases.json`

Check out `.example.airtable-api-key` and `bases.example.json` for examples.

## Example

Grab the `Name` field from table `MyTable` in the base labelled `MyBase` in
`bases.json`, and give back csv\* results:

    node ./at.js MyBase.MyTable.Name,Notes csv

You can specify the view to use with an at-sign after the table name. Use
quotes if any part of the query string has spaces:

    node ./at.js 'Beer Log.Beers@To Drink.Name' csv

You can specify multiple fields by separating them with commas

    node ./at.js 'Beer Log.Beers@Favorites.Name,Brewery,Drink Date' csv

`json`\*\* and `pretty` formats are also available

    node ./at.js 'Restaurant.Employees@Morning Shift.Name,Image' json
    node ./at.js 'My Shop.Items@In Stock.Name,Description,Price' pretty


\* I can't guarantee the CSV will be quoted properly. Would definitely be a
to-do if I worked on this project more.

\*\* The JSON is straight from the Airtable SDK, so whatever format they
provide is what you get.
